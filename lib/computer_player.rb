class ComputerPlayer

  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
    @mark = :O
  end

  def display(board)
    @board = board
  end

  def get_move
  end

end
