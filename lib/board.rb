class Board
  attr_accessor :grid, :winner

  def initialize(grid =
    [[nil, nil, nil],
    [nil, nil, nil],
    [nil, nil, nil]])

    @grid = grid
  end

  def place_mark(pos, mark)
    row = pos[0]
    column = pos[1]

    @grid[row][column] = mark
  end

  def empty?(pos)
    row = pos[0]
    column = pos[1]

    return true if @grid[row][column] == nil
    false
  end

  def over?
    return true if winner
    return true if grid.flatten.none? { |ele| ele == nil } && winner == nil
    false
  end

  def winner
      #rows
      return :X if grid[0].all? { |el| el == :X }
      return :X if grid[1].all? { |el| el == :X }
      return :X if grid[2].all? { |el| el == :X }
      return :O if grid[0].all? { |el| el == :O }
      return :O if grid[1].all? { |el| el == :O }
      return :O if grid[2].all? { |el| el == :O }
      #columns
      return :X if grid.each_index.all? { |idx| grid[idx][0] == :X }
      return :X if grid.each_index.all? { |idx| grid[idx][1] == :X }
      return :X if grid.each_index.all? { |idx| grid[idx][2] == :X }
      return :O if grid.each_index.all? { |idx| grid[idx][0] == :O }
      return :O if grid.each_index.all? { |idx| grid[idx][1] == :O }
      return :O if grid.each_index.all? { |idx| grid[idx][2] == :O }
      #diagonals
      return :X if grid.each_index.all? { |idx| grid[idx][idx] == :X}
      return :O if grid.each_index.all? { |idx| grid[idx][idx] == :O}
      return :X if grid[0][2] == :X && grid[1][1] == :X && grid[2][0] == :X
      return :O if grid[0][2] == :X && grid[1][1] == :X && grid[2][0] == :O
    end

  # def winner
  #   three_x = [:X, :X, :X]
  #   three_o = [:O, :O, :O]
  #   left_diagonal = [@grid[0][0], @grid[1][1], @grid[2][2]]
  #   right_diagonal = [@grid[2][0], @grid[1][1], @grid[0][2]]
  #
  #   # check rows
  #   return :X if @grid.any? { |ele| ele == three_x }
  #   return :O if @grid.any? { |ele| ele == three_o }
  #
  #   #check columns
  #   return :X if @grid.transpose.any? { |ele| ele == three_x }
  #   return :O if @grid.transpose.any? { |ele| ele == three_o }
  #
  #   #check diagonals
  #   return :X if left_diagonal == three_x || right_diagonal == three_x
  #   return :O if left_diagonal == three_o || right_diagonal == three_o
  # end

end
