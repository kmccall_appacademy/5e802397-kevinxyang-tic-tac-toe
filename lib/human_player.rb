class HumanPlayer

  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def get_move
    print "Where do you pick? Choose it like this row, column: "
    move = gets.chomp

    [move[0].to_i, move[-1].to_i]
  end

  def display(board)
    print board.grid
  end
end
