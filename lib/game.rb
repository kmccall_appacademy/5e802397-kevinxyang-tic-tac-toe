require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_reader :player_one, :player_two, :current_player, :board

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @current_player = player_one

    # FIGURE this out:
    @board = Board.new
  end

  def switch_players!
    if @current_player == player_one
      @current_player = player_two
    else
      @current_player = player_two
    end
  end

  def play_turn
    # WHY DIDN'T until board.over? WORK INSIDE THIS METHOD?
    # until board.over?
      current_player.display(board)
      the_move = current_player.get_move
      board.place_mark(the_move, current_player.mark)
      switch_players!
    # end
  end

  def play
    until board.over?
      play_turn
    end
    print board.grid
    puts ""
    puts "Congratulations!"
  end

end
